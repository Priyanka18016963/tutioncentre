package com.uh.tuitioncentre.model;

import java.util.List;

public class Tutor { 

	private String name;
	private List<Boolean> availabilityList;
	private List<String> subjectList;
	private String date;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public List<Boolean> getAvailabilityList() {
		return availabilityList;
	}
	public void setAvailabilityList(List<Boolean> availabilityList) {
		this.availabilityList = availabilityList;
	}
	public List<String> getSubjectList() {
		return subjectList;
	}
	public void setSubjectList(List<String> subjectList) {
		this.subjectList = subjectList;
	}
	
	
	
}
