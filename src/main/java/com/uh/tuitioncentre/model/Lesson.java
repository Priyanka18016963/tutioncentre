package com.uh.tuitioncentre.model;

import java.util.List;

public class Lesson {

	private String studentName;
	private String tutorName;
	private String subject;
	private String status;
	private String date;
	private int time;
	private List<Lesson> availableSlots; 
	
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getTutorName() {
		return tutorName;
	}
	public void setTutorName(String tutorName) {
		this.tutorName = tutorName;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int i) {
		this.time = i;
	}
	public List<Lesson> getAvailableSlots() {
		return availableSlots;
	}
	public void setAvailableSlots(List<Lesson> availableSlots) {
		this.availableSlots = availableSlots;
	}
	
	
}
