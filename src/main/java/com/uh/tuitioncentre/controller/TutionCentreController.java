package com.uh.tuitioncentre.controller;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.uh.tuitioncentre.model.Book;
import com.uh.tuitioncentre.model.Lesson;
import com.uh.tuitioncentre.model.Student;
import com.uh.tuitioncentre.model.Tutor;
import com.uh.tuitioncentre.service.BookingService;
import com.uh.tuitioncentre.service.BooksRecordService;
import com.uh.tuitioncentre.service.LoginService;
import com.uh.tuitioncentre.service.ReportService;

@Controller
@Scope("session")
public class TutionCentreController {
	public static final DateTimeFormatter UK_DATE_FORMATER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	@RequestMapping("/")
	public ModelAndView getStartPage() {
		ModelAndView mandv = new ModelAndView();
		mandv.setViewName("index");
		return mandv;
	}
	
	@Autowired
	LoginService loginService;
	
	
	@Autowired
	BookingService bookingService;
	
	@Autowired
	BooksRecordService booksRecordService;
	
	@Autowired
	ReportService reportService;
	
	@RequestMapping("/loginDetails")
	public ModelAndView loginDetails(@RequestParam("fullName") String fullName,
			@RequestParam("hiddenValue") String hiddenValue, HttpSession session) {
		ModelAndView mandv = new ModelAndView();
		
		try {
            System.out.println("hidden valus is " +hiddenValue);
			session.setAttribute("name", fullName);
			List<Lesson> lessonsList = bookingService.getBookings(fullName,hiddenValue);
			System.out.println("bookings list in controller" + lessonsList.size());
			
			if(hiddenValue.contentEquals("S")) {
				Student student = loginService.getStudentDetails(fullName, hiddenValue);
				List<Book> booksList = booksRecordService.getBooksListForStudent(fullName);
				mandv.addObject("model", student);
				mandv.addObject("model1", lessonsList);
				mandv.addObject("model2", booksList);
				mandv.setViewName("studentHomePage");
			}else if(hiddenValue.contentEquals("T")){
				Tutor tutor = loginService.getTutorDetails(fullName, hiddenValue);
				mandv.addObject("model", tutor);
				mandv.addObject("model1", lessonsList);
				mandv.setViewName("tutorHomePage");
			}else {
				mandv.setViewName("adminHomePage");
			}
			
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return mandv;
	}

	@RequestMapping("/bookClass")
	public ModelAndView bookAClass(@RequestParam("subject") String result, HttpSession session) {
		ModelAndView mandv = new ModelAndView();

		try {
			session.setAttribute("subject", result);
			Lesson lesson = bookingService.bookingLesson(result, (String) session.getAttribute("name"));
			mandv.addObject("model", lesson);
			mandv.setViewName("bookClassPage");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mandv;
	}

	@RequestMapping("/updatebooking")
	public ModelAndView updateBooking(@RequestParam("selectedSlot") String selectedSlot,@RequestParam("isCancel") String isCancel, HttpSession session) {
		ModelAndView mandv = new ModelAndView();

		try {
			System.out.println("called from change booking "+isCancel);
			Lesson lesson = new Lesson();
			lesson.setTutorName(selectedSlot.split(",")[0]);
			lesson.setDate(selectedSlot.split(",")[1]);
			lesson.setTime(Integer.parseInt(selectedSlot.split(",")[2]));
			lesson.setStatus(selectedSlot.split(",")[3]);
			lesson.setSubject(selectedSlot.split(",")[4]);
			lesson.setStudentName((String) session.getAttribute("name"));
			
			if (isCancel.equalsIgnoreCase("yes")) {
				lesson.setStatus("Cancelled");
				bookingService.confirmChangeBooking(lesson, lesson);
				mandv.addObject("cancelConfirmed", "Your lesson has been cancelled");
				mandv.setViewName("studentHomePage");
			}else if(isCancel.equalsIgnoreCase("no")){
				mandv.addObject("model", lesson);
				mandv.setViewName("bookClassPage");
			}else {
				lesson.setTutorName((String) session.getAttribute("name"));
				lesson.setStudentName(selectedSlot.split(",")[0]);
				lesson.setStatus("Attended");
				bookingService.confirmChangeBooking(lesson, lesson);
				mandv.setViewName("tutorHomePage");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mandv;
	}

	@RequestMapping("/changeBooking")
	public ModelAndView changeBooking(@RequestParam Map<String,String> requestParamMap,@RequestParam("selectedDate") @DateTimeFormat(iso = ISO.DATE) LocalDate dateSelected ,HttpSession session) {
		ModelAndView mandv = new ModelAndView();

		Lesson previousLesson = new Lesson();
		previousLesson.setSubject(requestParamMap.get("subject"));
		previousLesson.setDate(requestParamMap.get("date"));
		previousLesson.setTime(Integer.parseInt(requestParamMap.get("time")));
		previousLesson.setStatus(requestParamMap.get("status"));
		
		try {
			List<Lesson> availableSlots = bookingService.changeBooking(previousLesson.getSubject(),(String) session.getAttribute("name"), dateSelected);
		mandv.addObject("model", availableSlots);
			mandv.addObject("previousLesson", previousLesson);
			mandv.setViewName("changeBookingPage");
			
		} catch (Exception exception) {
			exception.printStackTrace();
			
		}

		return mandv;
	}

	@RequestMapping("/confirmBookingChange")
	public ModelAndView confirmBookingChange(@RequestParam Map<String,String> requestParamMap, HttpSession session) {
		Lesson oldLesson= new Lesson();
		Lesson newLesson= new Lesson();
		ModelAndView mandv = new ModelAndView();
		try {
		
			oldLesson.setDate(requestParamMap.get("previousDate"));
			oldLesson.setTime(Integer.parseInt(requestParamMap.get("previousTime")));
			oldLesson.setSubject(requestParamMap.get("previousSubject"));
			
			String selectedSlot= requestParamMap.get("selectedSlot");
			
			newLesson.setDate(selectedSlot.split(",")[1]);
			newLesson.setSubject(oldLesson.getSubject());
			newLesson.setTime(Integer.parseInt(selectedSlot.split(",")[2]));
			newLesson.setTutorName(selectedSlot.split(",")[0]);
			newLesson.setStatus("Changed");
			newLesson.setStudentName((String) session.getAttribute("name"));
			
			bookingService.confirmChangeBooking(oldLesson, newLesson);
			
			mandv.addObject("model", newLesson);
			mandv.setViewName("bookClassPage");
			
		}catch(Exception exception) {
			exception.printStackTrace();
			
		}
		
		return mandv;
	}
	
	@RequestMapping("/displayBooksList")
	public ModelAndView displayBooksList(@RequestParam("selectedSlot") String selectedSlot) {
		ModelAndView mandv = new ModelAndView();

		try {
			List<String> booksList = booksRecordService.getBooksList(selectedSlot.split(",")[0]);
			System.out.println("books list in controller*******" +booksList);
			mandv.addObject("model", booksList);
			mandv.addObject("model1", selectedSlot.split(",")[0]);
			mandv.setViewName("displayBooksPage");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mandv;
	}
	
	@RequestMapping("/confirmAddBook")
	public ModelAndView confirmAddBook(@RequestParam("studentname") String studentName,@RequestParam("selectedSlot") String selectedSlot) {
		ModelAndView mandv = new ModelAndView();

		try {
			booksRecordService.confirmAddBooks(studentName, selectedSlot);
			System.out.println("book selected in controller&&&&&&&&&" +selectedSlot);
			mandv.setViewName("tutorHomePage");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mandv;
	}

	@RequestMapping("/retrieveBookedLessons")
	public ModelAndView retrieveBookedLessons(@RequestParam("bookedlessons") String month) {
		ModelAndView mandv = new ModelAndView();

		System.out.println("value of month is "+month);
		try {
			List<Lesson> lessonsList = reportService.retrieveBookedLessons(month);
			mandv.addObject("model", lessonsList);
			mandv.setViewName("displayLessonsReport");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mandv;
	}

	@RequestMapping("/retrieveAttendedLessons")
	public ModelAndView retrieveAttendedLessons(@RequestParam("attendedlessons") String month) {
		ModelAndView mandv = new ModelAndView();

		try {
			List<Lesson> lessonsList = reportService.retrieveAttendedLessons(month);
			mandv.addObject("model", lessonsList);
			mandv.setViewName("displayLessonsReport");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mandv;
	}
	
	
	@RequestMapping("/retrieveBooksList")
	public ModelAndView retrieveBooksList(@RequestParam("studentName") String studentName) {
		ModelAndView mandv = new ModelAndView();
		
		try {
			List<Book> lessonsList = booksRecordService.getBooksListForStudent(studentName);
			mandv.addObject("model", lessonsList);
			mandv.setViewName("displayBooksReport");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mandv;
	}

	@RequestMapping("/updateBooksOrdered")
	public ModelAndView updateBooksOrdered(@RequestParam("selectedSlot") String studentName) {
		ModelAndView mandv = new ModelAndView();

		try {
			reportService.updateBookStatusAsOrdered(studentName);
			mandv.setViewName("displayBooksReport");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mandv;
	}


}
