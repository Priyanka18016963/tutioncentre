package com.uh.tuitioncentre.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import com.uh.tuitioncentre.model.Lesson;

public class DataLoader {
	
	public String bookingsFilePath = "D:\\bookedLessons.txt";
	public String booksFilePath = "D:\\booksList.txt";
	@Autowired
	ServletContext servletctx;

	public List<String> getDataFromDataFile(String name, String token) throws IOException {
		List<String> genericList = new ArrayList<String>();
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("data");
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			Stream<String> stream = br.lines();
			if (token.equals("S")) {
				genericList = stream.filter(line -> line.startsWith("S")).collect(Collectors.toList());
			} else {
				genericList = stream.filter(line -> line.startsWith("T")).collect(Collectors.toList());
			}
		}
		return genericList;
	}

	public Map<String, String> getTimeTable(String subject) throws IOException {
		Map<String, String> timetableMap = new HashMap<String, String>();

		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("timetable");

		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			Stream<String> stream = br.lines();

			timetableMap = stream.filter(str -> str.toString().split("/")[0].equalsIgnoreCase(subject)).collect(
					Collectors.toMap(str -> str.toString().split("/")[0], str -> str.toString().split("/")[1]));
			System.out.println(timetableMap);
		}
		return timetableMap;
	}

	public Map<String, String> getBookings(String date, String subject) throws IOException {
		Map<String, String> bookingsMap = new HashMap<String, String>();

		FileInputStream inputStream = new FileInputStream(bookingsFilePath);
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			Stream<String> stream = br.lines();
			bookingsMap = stream
					.filter(str -> (str.toString().split(",")[0].equalsIgnoreCase(date)
							&& str.toString().split(",")[1].equalsIgnoreCase(subject)))
					.collect(Collectors.toMap(str -> str.toString().split(",")[1], str -> str.toString().split(",")[2],
							(oldValue, newValue) -> new StringBuilder(oldValue).append(":").append(newValue)
									.toString()));
			System.out.println(bookingsMap);
		}
		return bookingsMap;
	}

	public String getBookingByDateAndTime(String date, String subject, String time) throws IOException {
		String result = null;

		FileInputStream inputStream = new FileInputStream(bookingsFilePath);

		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {

			Stream<String> stream = br.lines();

			List<String> bookedList = stream.filter(str -> (str.toString().split(",")[0].equalsIgnoreCase(date)
					&& str.toString().split(",")[1].equalsIgnoreCase(subject)
					&& str.toString().split(",")[2].equalsIgnoreCase(time))).collect(Collectors.toList());

			if (bookedList != null && bookedList.size() > 0) {
				result = bookedList.get(0);
			}

		}
		return result;
	}

	public boolean deleteBooking(String date, String subject, String time) throws IOException {
		boolean isdeleted = false;
		//File file = ResourceUtils.getFile("classpath:bookedLessons.txt");
		File file =  new File(bookingsFilePath);
		FileInputStream inputStream = new FileInputStream(file);
		Charset utf8 = StandardCharsets.UTF_8;

		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {

			Stream<String> stream = br.lines();

			Collection<String> bookingsList = stream.collect(Collectors.toList());

			bookingsList.removeIf(str -> (str.toString().split(",")[0].equalsIgnoreCase(date)
					&& str.toString().split(",")[1].equalsIgnoreCase(subject)
					&& str.toString().split(",")[2].equalsIgnoreCase(time)));

			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, false), utf8));
			for (String booking : bookingsList) {
				writer.write(booking + "\n");
			}
			writer.close();
			isdeleted = true;
		}
		return isdeleted;
	}

	public void writeBookedLesson(Lesson lesson) throws IOException {
		Charset utf8 = StandardCharsets.UTF_8;
		
		File file =  new File(bookingsFilePath);
		
		StringBuilder bookedlessonDetails = new StringBuilder();

		bookedlessonDetails.append(lesson.getDate()).append(",").append(lesson.getSubject()).append(",")
				.append(lesson.getTime()).append(",").append(lesson.getTutorName()).append(",")
				.append(lesson.getStudentName()).append(",").append(lesson.getStatus()).append("\n");
		System.out.println("initial:::::"+ bookedlessonDetails);
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), utf8))) {
			
			writer.write(bookedlessonDetails.toString());
			System.out.println("details:::::" +bookedlessonDetails.toString());
		}
	}

	public List<String> getBookingsWithName(String name, String isStudent) throws IOException {
		List<String> bookingsList = new ArrayList<String>();

		FileInputStream inputStream = new FileInputStream(bookingsFilePath);
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			Stream<String> stream = br.lines();
			if (isStudent.equalsIgnoreCase("S")) {
				bookingsList = stream.filter(str -> (str.toString().split(",")[4].equalsIgnoreCase(name)))
						.collect(Collectors.toList());
			} else {
				bookingsList = stream.filter(str -> (str.toString().split(",")[3].equalsIgnoreCase(name)))
						.collect(Collectors.toList());
			}
			System.out.println(bookingsList);
		}
		return bookingsList;
	}

	public List<String> getBooksList(String studentName) throws IOException {
		List<String> booksList = new ArrayList<String>();

		//File file = ResourceUtils.getFile("classpath:booksList.txt");
		FileInputStream inputStream = new FileInputStream(new File(booksFilePath));
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			Stream<String> stream = br.lines();
			booksList = stream.filter(str -> (str.toString().split("/")[0].equalsIgnoreCase(studentName)))
					.collect(Collectors.toList());
			System.out.println(booksList);
		}
		return booksList;

	}

	public void addBooks(String studentName, String bookName) throws IOException {
		Charset utf8 = StandardCharsets.UTF_8;
		//File file = ResourceUtils.getFile("classpath:booksList.txt");
		File file = new File(booksFilePath);
		FileInputStream inputStream = new FileInputStream(file);
		List<String> booksList = new ArrayList<String>();
		List<String> updatedList = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			Stream<String> stream = br.lines();
			booksList = stream.collect(Collectors.toList());

			if (booksList.stream()
					.anyMatch(eachStudentRecord -> eachStudentRecord.split("/")[0].equalsIgnoreCase(studentName))) {
				updatedList = booksList.stream().map(temp -> {
					String result = temp;
					if (temp.split("/")[0].equalsIgnoreCase(studentName)) {
						result = temp + "," + bookName;
					}
					return result;
				}).collect(Collectors.toList());
			} else {
				updatedList.addAll(booksList);
				updatedList.add(studentName + "/Requested/" + bookName);
			}
			System.out.println("UpdatedList::::" + updatedList);

		}

		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, false), utf8))) {
			for (String eachRecord : updatedList) {
				writer.write(eachRecord + "\n");
			}

		}
	}

	public List<Lesson> retrieveBookedLessons(String month) throws IOException {

		//File file = ResourceUtils.getFile("classpath:bookedLessons.txt");
		FileInputStream inputStream = new FileInputStream(bookingsFilePath);
		List<Lesson> resultList = new ArrayList<Lesson>();
		List<String> bookedLessonsList = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			Stream<String> stream = br.lines();
			bookedLessonsList = stream.filter(record -> (record.split(",")[0].split("/")[1].equalsIgnoreCase(month)
					&& record.split(",")[5].equalsIgnoreCase("Booked"))).collect(Collectors.toList());
			resultList = bookedLessonsList.stream().map(record -> {
				Lesson lesson = new Lesson();
				lesson.setDate(record.split(",")[0]);
				lesson.setSubject(record.split(",")[1]);
				lesson.setTime(Integer.parseInt(record.split(",")[2]));
				lesson.setTutorName(record.split(",")[3]);
				lesson.setStudentName(record.split(",")[4]);
				lesson.setStatus(record.split(",")[5]);
				return lesson;

			}).collect(Collectors.toList());

			System.out.println(bookedLessonsList);
		}
		return resultList;

	}

	public List<Lesson> retrieveAttendedLessons(String month) throws IOException {

		//File file = ResourceUtils.getFile("classpath:bookedLessons.txt");
		FileInputStream inputStream = new FileInputStream(bookingsFilePath);
		List<Lesson> resultList = new ArrayList<Lesson>();
		List<String> bookedLessonsList = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			Stream<String> stream = br.lines();
			bookedLessonsList = stream.filter(record -> (record.split(",")[0].split("/")[1].equalsIgnoreCase(month)
					&& (record.split(",")[5].equalsIgnoreCase("Attended") || record.split(",")[5].equalsIgnoreCase("Cancelled") 
							|| record.split(",")[5].equalsIgnoreCase("Changed")))).
					collect(Collectors.toList());
			resultList = bookedLessonsList.stream().map(record -> {
				Lesson lesson = new Lesson();
				lesson.setDate(record.split(",")[0]);
				lesson.setSubject(record.split(",")[1]);
				lesson.setTime(Integer.parseInt(record.split(",")[2]));
				lesson.setTutorName(record.split(",")[3]);
				lesson.setStudentName(record.split(",")[4]);
				lesson.setStatus(record.split(",")[5]);
				return lesson;

			}).collect(Collectors.toList());

			System.out.println(bookedLessonsList);
		}
		return resultList;

	}

	public void updateBooksStatus(String studentName) throws IOException {
		Charset utf8 = StandardCharsets.UTF_8;
		//File file = ResourceUtils.getFile("classpath:booksList.txt");
		FileInputStream inputStream = new FileInputStream(new File(booksFilePath));
		List<String> updatedList = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			Stream<String> stream = br.lines();
			List<String> booksList = stream.collect(Collectors.toList());
			updatedList = booksList.stream().map(record -> {
				String result = record;
				if (record.split("/")[0].toString().equalsIgnoreCase(studentName)) {
					result = record.split("/")[0] + "/Ordered/" + record.split("/")[2];
				}
				return result;
			}).collect(Collectors.toList());

		}

		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(booksFilePath, false), utf8))) {
			for (String eachRecord : updatedList) {
				writer.write(eachRecord + "\n");
			}

		}

	}

	public static void main(String[] args) throws IOException {
		DataLoader dl = new DataLoader();
		// dl.writeBookedLesson("Jose","27/11/2019","1");

		// dl.getBookings("02/12/2019","1");
		// dl.getBookingsForStudent("Jack");

		// dl.deleteBooking("09/12/2019","1","16");
		/*
		 * List<Lesson> result = dl.retrieveBookedLessons("01"); int i = 0; for (Lesson
		 * lesson : result) { i++; System.out.println("Record count:::" + i);
		 * System.out.println("value1:::" + lesson.getDate());
		 * System.out.println("value1::" + lesson.getStatus());
		 * System.out.println("value1:::" + lesson.getStudentName()); }
		 * System.out.println("added");
		 */
		
	Lesson lesson = new Lesson();
	lesson.setDate("12/10.2019");
	lesson.setTime(16);
	lesson.setSubject("1");
	lesson.setStudentName("Jack");
	lesson.setStatus("Booked");
	lesson.setTutorName("Tom");
	dl.writeBookedLesson(lesson);
	System.out.println("added");
	}

}
