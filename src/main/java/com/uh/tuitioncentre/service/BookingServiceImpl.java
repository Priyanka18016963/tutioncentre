package com.uh.tuitioncentre.service;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.uh.tuitioncentre.model.Lesson;
import com.uh.tuitioncentre.util.DataLoader;

@Service
public class BookingServiceImpl implements BookingService {
	
	public static final DateTimeFormatter UK_DATE_FORMATER=DateTimeFormatter.ofPattern("dd/MM/yyyy");

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.uh.tuitioncentre.service.BookingService#bookingLesson(java.lang.String,
	 * java.lang.String) 
	 */
	@Override
	public Lesson bookingLesson(String subject, String studentName) throws IOException, ParseException {
		String tutorNameAndSlot = null;
		Lesson lesson = new Lesson();
		DataLoader data = new DataLoader();
		int currentHour=LocalTime.now().getHour();
		LocalDate actualDate=LocalDate.now();
		if ((actualDate.getDayOfWeek().getValue() >=5) || currentHour >= 18) {
			
			do {
		    	actualDate = actualDate.plusDays(1);
		    } while(actualDate.getDayOfWeek().getValue() >= 5);
			
		}
		String availableDate=actualDate.format(UK_DATE_FORMATER).toString();
		System.out.println("Date After do Method And before calling get availabletutor Method :::"+availableDate);
		tutorNameAndSlot=getNextAvailableTutorAndTime(availableDate, subject);
		
		
		lesson.setStatus("Booked");
		lesson.setSubject(subject);
		lesson.setStudentName(studentName);
		lesson.setTutorName(tutorNameAndSlot.split(":")[0]);
		lesson.setTime(Integer.parseInt(tutorNameAndSlot.split(":")[1]));
		lesson.setDate(tutorNameAndSlot.split(":")[2]);
		data.writeBookedLesson(lesson);
		return lesson;
	}
	
	@Override
	public List<Lesson> changeBooking(String subject, String name, LocalDate date) throws IOException, ParseException {
		
		/** Fixed Time Slot Indexes from 4 PM to 8 PM **/
		Map<Integer, Integer> timeMap = new HashMap<Integer, Integer>();
		timeMap.put(0, 16);
		timeMap.put(1, 17);
		timeMap.put(2, 18);
		timeMap.put(3, 19);

		List<Lesson> availableSlots = new ArrayList<Lesson>();
		String selectedDate=date.format(UK_DATE_FORMATER).toString();
		Map<String, List<String>> availableSlotsMap = getAvailableSlots(selectedDate, subject);
		List<String> scheduledSlotsList = availableSlotsMap.get(subject);
		for (String eachSlot : scheduledSlotsList) {
			if (!eachSlot.equalsIgnoreCase("b")) {
				Lesson lesson = new Lesson();
				lesson.setDate(selectedDate);
				lesson.setStudentName(name);
				lesson.setSubject(subject);
				lesson.setTutorName(eachSlot);
				lesson.setTime(timeMap.get(scheduledSlotsList.indexOf(eachSlot)));
				availableSlots.add(lesson);
			}
		}
		
		return availableSlots;
	}
	
	@Override
	public List<Lesson> getBookings(String name,String isStudent) throws IOException { 
		DataLoader data = new DataLoader();
		List<Lesson> finalLessonsList = new ArrayList<Lesson>();
		List<String> lessonsList = data.getBookingsWithName(name,isStudent);
		System.out.println("lessons list is " +lessonsList);
		for(String lessons:lessonsList) {
			Lesson lesson = new Lesson();
			lesson.setDate(lessons.split(",")[0]);
			lesson.setStatus(lessons.split(",")[5]);
			lesson.setStudentName(lessons.split(",")[4]);
			lesson.setSubject(lessons.split(",")[1]);
			lesson.setTime(Integer.parseInt(lessons.split(",")[2]));
			lesson.setTutorName(lessons.split(",")[3]);
			finalLessonsList.add(lesson);
		}
		System.out.println("final lessons list is " +finalLessonsList.size());
		return finalLessonsList;
	}
	
	
	@Override
	public Lesson confirmChangeBooking(Lesson oldSesson,Lesson newLesson) throws IOException {
		DataLoader dataLoader= new DataLoader();
		String time=""+oldSesson.getTime();
		boolean isdeleted=dataLoader.deleteBooking(oldSesson.getDate(), oldSesson.getSubject(), time);
		if (isdeleted) {
			dataLoader.writeBookedLesson(newLesson);
		}
		return newLesson;
	}
	
	public static Map<String, List<String>> getAvailableSlots(String date, String subject)
			throws IOException, ParseException {

		/** Fixed Time Slot Indexes from 4 PM to 8 PM **/
		Map<String, Integer> timeMap = new HashMap<String, Integer>();
		timeMap.put("16", 0);
		timeMap.put("17", 1);
		timeMap.put("18", 2);
		timeMap.put("19", 3);

		DataLoader data = new DataLoader();

		Map<String, List<String>> availabilityMap = new HashMap<String, List<String>>();
		Map<String, List<String>> scheduledMap = new HashMap<String, List<String>>();
		Map<String, List<String>> bookingsMap = new HashMap<String, List<String>>();
		
		/** Add Time table and booking data to the respective Maps**/
		Map<String, String> subjectScheduleMap = data.getTimeTable(subject);
		System.out.println("schedled map is "+ subjectScheduleMap);
		Map<String, String> subjectBookings = data.getBookings(date, subject);
		System.out.println("bookings done " +subjectBookings);

		/** Fetch Day from the Date**/
		
		System.out.println("Date is " +date);
		int dayOfWeek =LocalDate.parse(date, UK_DATE_FORMATER).getDayOfWeek().getValue();
		
        System.out.println("day of week is " +dayOfWeek);
		

		/** Time table for the subject **/
		for (Map.Entry<String, String> mapEntry : subjectScheduleMap.entrySet()) {
			scheduledMap.put(mapEntry.getKey(), Arrays.asList(mapEntry.getValue().split(",")[dayOfWeek-1].split(":")));
		}
		/** bookings for the subject **/
		for (Map.Entry<String, String> mapEntry : subjectBookings.entrySet()) {
			bookingsMap.put(mapEntry.getKey(), Arrays.asList(mapEntry.getValue().split(":")));
		}

		/** Available bookings (time table minus bookings) **/
		for (Map.Entry<String, List<String>> scheduledEntry : scheduledMap.entrySet()) {

			List<String> scheduledSlotsList = scheduledEntry.getValue();
			LinkedList<String> availableSlotsList = new LinkedList<String>(scheduledSlotsList);

			List<String> bookedSlotsList = new ArrayList<String>();

			for (Map.Entry<String, List<String>> bookedEntry : bookingsMap.entrySet()) {
				bookedSlotsList = bookedEntry.getValue();
			}

			/** Updating the available slots with the booked slots **/
			for (String bookedTime : bookedSlotsList) {
				int index = timeMap.get(bookedTime);
				availableSlotsList.remove(index);
				availableSlotsList.add(index,"b");
			}

			availabilityMap.put(scheduledEntry.getKey(), availableSlotsList);

		}

		System.out.println("Availabilty::::::::::" + availabilityMap);

		return availabilityMap;

	}
	
	private String getNextAvailableTutorAndTime(String date,String subject) throws IOException, ParseException {
		StringBuilder result=new StringBuilder();
		int tutorTimeSlot=0;
		String tutor=null;
		/** Fixed Time Slot Indexes from 4 PM to 8 PM **/
		Map<Integer, Integer> timeMap = new HashMap<Integer, Integer>();
		timeMap.put(0, 16);
		timeMap.put(1, 17);
		timeMap.put(2, 18);
		timeMap.put(3, 19);
		
		String currentDate = LocalDate.now().format(UK_DATE_FORMATER).toString();
		int currentHour=LocalTime.now().getHour();
		System.out.println("Available Date is ::::::::::::::::::::::" +date );
		Map<String, List<String>> availableSlots = getAvailableSlots(date, subject);
		List<String> availableSlotsList = availableSlots.get(subject);
		for (String slot : availableSlotsList) {
			if (!slot.equalsIgnoreCase("b")) {
				if (date == currentDate && currentHour <=(timeMap.get(availableSlotsList.indexOf(slot)-1))) {
					tutor = slot;
					tutorTimeSlot=timeMap.get(availableSlotsList.indexOf(slot));
					break;
				}else {
					tutor = slot;
					tutorTimeSlot=timeMap.get(availableSlotsList.indexOf(slot));
					break;
				}
				
			}
		}
		
		if (tutor == null) {
			System.out.println("As slots are booked trying to fetch slots for Next day");
			LocalDate date1= LocalDate.parse(date, UK_DATE_FORMATER);
			do {
				date1 = date1.plusDays(1);
		    } while(date1.getDayOfWeek().getValue() >= 5);
			date= date1.format(UK_DATE_FORMATER).toString() ;
			String finalResult=getNextAvailableTutorAndTime(date,subject);
			return finalResult;
		}
		System.out.println("Available Date ::"+date +"::Available Tutor:: "+ tutor + "::Available Time::" +tutorTimeSlot);
		result.append(tutor).append(":").append(tutorTimeSlot).append(":").append(date);
		return result.toString();
		
	}

}
