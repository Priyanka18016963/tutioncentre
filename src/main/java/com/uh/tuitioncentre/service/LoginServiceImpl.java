package com.uh.tuitioncentre.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.uh.tuitioncentre.model.Student;
import com.uh.tuitioncentre.model.Tutor;
import com.uh.tuitioncentre.util.DataLoader;

@Service
public class LoginServiceImpl implements LoginService { 

	@Override
	public Student getStudentDetails(String fullname, String token) throws IOException {
		DataLoader data = new DataLoader();
		Student student = new Student();
		List<String> studentList = data.getDataFromDataFile(fullname, token);
		for(String student1:studentList) {
			String[] studentDetails = student1.split(",");
			if(fullname.equalsIgnoreCase(studentDetails[1])) {
				student.setName(studentDetails[1]);
				student.setAddress(studentDetails[4]);
				student.setContactNumber(studentDetails[5]);
				student.setDob(studentDetails[3]);
				student.setGender(studentDetails[2]);
			}
		}
		return student;
	}
	
	@Override
	public Tutor getTutorDetails(String fullname, String token) throws IOException {
		DataLoader data = new DataLoader();
		Tutor tutor = new Tutor();
		List<String> studentList = data.getDataFromDataFile(fullname, token);
		for(String student1:studentList) {
			String[] studentDetails = student1.split(",");
			if(fullname.equalsIgnoreCase(studentDetails[1])) {
				tutor.setName(studentDetails[1]);
			}
		}
		return tutor;
	}

}
