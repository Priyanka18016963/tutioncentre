package com.uh.tuitioncentre.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.uh.tuitioncentre.model.Lesson;

@Service
public interface ReportService {

	public List<Lesson> retrieveBookedLessons(String month) throws IOException;
	
	public List<Lesson> retrieveAttendedLessons(String month) throws IOException;

	 public void updateBookStatusAsOrdered(String studentName) throws IOException;
}
