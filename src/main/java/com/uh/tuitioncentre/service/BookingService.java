package com.uh.tuitioncentre.service;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Service;

import com.uh.tuitioncentre.model.Lesson;

@Service
public interface BookingService { 

	public Lesson bookingLesson(String subject,String name) throws IOException, ParseException;
	
	public Lesson confirmChangeBooking(Lesson oldLesson,Lesson newLesson)throws IOException;

	public List<Lesson> changeBooking(String subject, String name, LocalDate date) throws IOException, ParseException;
	
	public List<Lesson> getBookings(String studentName, String isStudent) throws IOException;
	
}
