package com.uh.tuitioncentre.service;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.uh.tuitioncentre.model.Student;
import com.uh.tuitioncentre.model.Tutor;

@Service
public interface LoginService { 

	public Student getStudentDetails(String fullname, String token) throws IOException;

	public Tutor getTutorDetails(String fullname, String token) throws IOException;
}
