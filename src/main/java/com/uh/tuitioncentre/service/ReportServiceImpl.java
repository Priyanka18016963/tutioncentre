package com.uh.tuitioncentre.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.uh.tuitioncentre.model.Lesson;
import com.uh.tuitioncentre.util.DataLoader;

@Service
public class ReportServiceImpl implements ReportService {

	@Override
	public List<Lesson> retrieveBookedLessons(String month) throws IOException {
		
		DataLoader data = new DataLoader();
		List<Lesson> lessonsList = data.retrieveBookedLessons(month);
		return lessonsList;
	}

	@Override
	public List<Lesson> retrieveAttendedLessons(String month) throws IOException {
		
		DataLoader data = new DataLoader();
		List<Lesson> lessonsList = data.retrieveAttendedLessons(month);
		return lessonsList;
	}
	
	@Override
	public void updateBookStatusAsOrdered(String studentName) throws IOException {
		
		DataLoader data = new DataLoader();
		data.updateBooksStatus(studentName);
	}

}
