package com.uh.tuitioncentre.service;

import java.io.IOException;
import java.util.List;

import com.uh.tuitioncentre.model.Book;

public interface BooksRecordService {

	public List<String> getBooksList(String studentName) throws IOException;

	public void confirmAddBooks(String studentName, String bookName) throws IOException;

	public List<Book> getBooksListForStudent(String studentName) throws IOException;

}
