package com.uh.tuitioncentre.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.uh.tuitioncentre.model.Book;
import com.uh.tuitioncentre.util.DataLoader;

@Service
public class BooksRecordServiceImpl implements BooksRecordService {

	@Override
	public List<String> getBooksList(String studentName) throws IOException {
		
		DataLoader data = new DataLoader();
		
		List<String> booksList = new ArrayList<String>();
		List<String> totalBooksList = new ArrayList<String>();
		totalBooksList.add("English Comprehension");
		totalBooksList.add("English Writing");
		totalBooksList.add("Math");
		totalBooksList.add("Numerical Reasoning");
		totalBooksList.add("Verbal Reasoning");
		totalBooksList.add("Non-verbal Reasoning");
		
		List<String> studentRecord = data.getBooksList(studentName);
		for(String books:studentRecord) {
			String oldBooks = books.split("/")[2];
			booksList.add(oldBooks);
			
		}
		Collection<String> resultList = totalBooksList;
		resultList.removeAll(booksList);
		
		
		System.out.println("booksList list is " +booksList);
		return new ArrayList<String>(resultList);

	}

	@Override
	public void confirmAddBooks(String studentName, String bookName) throws IOException {
		
		DataLoader data = new DataLoader();
		data.addBooks(studentName, bookName);

	}
	
	@Override
	public List<Book> getBooksListForStudent(String studentName) throws IOException {
		
		DataLoader data = new DataLoader();
		List<Book> booksList = new ArrayList<Book>();
		List<String> studentRecord = data.getBooksList(studentName);
		for(String books:studentRecord) {
			Book book = new Book();
			book.setBookName(books.split("/")[2]);
			book.setStatus(books.split("/")[1]);
			book.setStudentName(studentName);
			booksList.add(book);
			
		}
		return booksList;

	}


}
