package com.uh.tuitioncentre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class TuitioncentreApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(TuitioncentreApplication.class, args); 
	}
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(TuitioncentreApplication.class);
    }

}
