<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Booking Details</title>
<script type="text/javascript">
	function displayBookClass() {
		document.getElementById("changeBookingform").style.display="";
	}
	function restrictWeekends(){
		var input=document.getElementById("selectedDate").value;
		var inputValue=new Date(input).getUTCDay();
		if(inputValue==0 || inputValue==6){
			alert("Classes are not conducted on weekends. Please select from Monday till Friday");
		}else{
			document.getElementById("changeBookingForm").action = "/changeBooking";
			document.getElementById("changeBookingForm").submit();
		}
	}
	function disablePreviousDaySelection(){
		alert("inside");
		var today = new Date().toISOString().split('T')[0];
		document.getElementsByName("changeBookingDate")[0].setAttribute('min', today);
	}
</script>
</head>
<body>
	<form id="changeBookingForm" method="post">
	<input type="hidden" name ="date" value="${model.date}"/>
	<input type="hidden" name ="time" value="${model.time}"/>
	<input type="hidden" name ="subject" value="${model.subject}"/>
	<input type="hidden" name ="status" value="${model.status}"/>
		<div class="bookClassform" align="center">
			<h1>Your lesson is booked with the below details</h1>
			<table>
				<tr>
					<td>Tutor name</td>
					<td>${model.tutorName}</td>
				</tr>
				<tr>
					<td>Date</td>
					<td>${model.date}</td>
				</tr>
				<tr>
					<td>Time</td>
					<td>${model.time}</td> 
				</tr>
				<tr>
					<td>Status</td>
					<td>${model.status}</td>
				</tr>
			</table>
		</div>
		<div id="changeBooking" class="changeBooking" align="left">
			<table>
				<tr>
					<td>Change this booking</td>
					<td><button type="button" onclick="displayBookClass()">Change the lesson</button></td>
				</tr>
			</table>
		</div>
		<div id="changeBookingform" class="changeBookingform" align="left" style="display: none ">
			<table>
				<tr>
					<td>Change this lesson(date and time):</td>
					<td><input type="date" name="selectedDate" id="selectedDate" onload="disablePreviousDaySelection()"></td>
				</tr>
				<tr>
				<td><input type="button" value="Submit" onclick="restrictWeekends()"></td>
				</tr>
			</table>
		</div>
	</form>

</body>
</html>