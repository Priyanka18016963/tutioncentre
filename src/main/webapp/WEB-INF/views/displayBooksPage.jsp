<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Display Books List</title>
</head>
<body>

<div id="displaybookslist" class="displaybookslist">
		<form id="bookslistform" method="post" action="/confirmAddBook">
			<input type="hidden" name="studentname" id="studentname" value=${model1} />
			<c:choose>
				<c:when test="${empty model}">
					<h2>No Books Are Available</h2>
				</c:when>
				<c:otherwise>
					<div class="changeBookingDiv" align="center">
						<h2>List of books</h2>
						<table class="table table-striped">
							<thead>
								<tr class="tr tr-success">
									<td><h3>Book Name</h3></td>									
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${model}" var="temp">
									<tr>
										<td>${temp}</td>
										<td><input type="radio" id="selectedSlot"
											name="selectedSlot"
											value=${temp}>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<input type="submit" value="Confirm"> 
							
					</div>
				</c:otherwise>
			</c:choose>
		</form>
	</div>
</body>
</html>