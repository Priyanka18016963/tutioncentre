<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Extra Tuition Centre</title>
<script type="text/javascript">
	function displayLogin(hiddenValue) {
		document.getElementById("loginform").style.display="";
		document.getElementById("hiddenValue").value=hiddenValue;
	}
</script>
<style>
.loginform{  
    padding:10px;  
    border:1px solid pink;  
    border-radius:10px;  
    width:300px;
    margin-top:10px;  
} 
.indexpage{  
    padding:10px;  
    border:1px solid pink;  
    border-radius:10px;  
    width:300px;
    margin-top:10px;  
} 
.formheading{  
    background-color:red;  
    color:white;  
    padding:4px;  
    text-align:center;  
}  
.sub{  
background-color:red;  
padding: 7px 40px 7px 40px;  
color:white;  
font-weight:bold;  
margin-left:70px;  
border-radius:5px;  
}  
</style>
</head>
<body>
  <h1>Welcome To The Study Centre</h1>
  
  <div class="indexpage" align="center">
      <table>
        <tr>
          <td><input type="button" id="loginAsStudent" name="loginAsStudent" value="Login As Student" onclick="displayLogin('S')"></td>
         </tr>
         <tr>
          <td><input type="button" id="loginAsTutor" name="loginAsTutor" value="Login As Tutor" onclick="displayLogin('T')"></td>
         </tr> 
         <tr>
          <td><input type="button" id="loginAsAdmin" name="loginAsAdmin" value="Login As Admin" onclick="displayLogin('A')"></td>
         </tr>
      </table>
  </div>

	<div id="loginform" class="loginform" align="center" style="display: none">
	<h3 class="formheading">Please Login</h3>
    <form action="/loginDetails" method="post">
    <input type="hidden" name="hiddenValue" id="hiddenValue">
      <table>
        <tr>
          <td>Enter Your name</td>
          <td><input type="text" id="fullName" name="fullName"></td>
         </tr>
         <tr>
          <td>Enter Your Password</td>
          <td><input type="password" id="pwd" name="pwd"></td>
         </tr> 
         <tr>
          <td colspan="2" style="text-align:center"><input type="submit" value="Submit"></td>
        </tr>
      </table>
    </form>
  </div>

</body>
</html>