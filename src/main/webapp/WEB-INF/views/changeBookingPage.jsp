<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Change Booking</title>
</head>
<body>
<form id="changeBookingForm" method="post" action="confirmBookingChange">

	<input type="hidden" name ="previousDate" value="${previousLesson.date}"/>
	<input type="hidden" name ="previousTime" value="${previousLesson.time}"/>
	<input type="hidden" name ="previousSubject" value="${previousLesson.subject}"/>


		<c:choose>
			<c:when test="${empty model}">
				<h2>No Classes Available</h2>
			</c:when>
			<c:otherwise>
				<div class="changeBookingDiv" align="center">
		       <h2>Please select any one of the available slots for today</h2>
			<table class="table table-striped">
            <thead>
                <tr class="tr tr-success">
                    <td><h3>Tutor Name</h3></td>
                    <td><h3>Date</h3></td>
                    <td><h3>Time</h3></td>
                    <td><h3>Status</h3></td>
                    
                    
                </tr>   
            </thead>
            <tbody>
                <c:forEach items="${model}" var="temp">
                    <tr>
                        <td>${temp.tutorName}</td>
                        <td>${temp.date}</td>
                        <td>${temp.time}</td>
                        <td>${temp.status}</td>
                        <td>
                            <input type="radio" id="selectedSlot" name="selectedSlot" value=${temp.tutorName},${temp.date},${temp.time}>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <input type="submit" value="Submit">
		</div>
			</c:otherwise>
		</c:choose>
		
	</form>

</body>
</html>