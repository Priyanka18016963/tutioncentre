<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Monthly Report for Lessons</title>
</head>
<body>

<div id="displaybooksList" class="displaybooksList">
		<form id="displaybooksform" method="post">
			<c:choose>
				<c:when test="${empty model}">
					<h2>No Lessons Are Booked</h2>
				</c:when>
				<c:otherwise>
					<div class="changeBookingDiv" align="center">
						<h2>List of lessons</h2>
						<table class="table table-striped">
							<thead>
								<tr class="tr tr-success">
								<td><h3>Student Name</h3></td>
								<td><h3>Tutor Name</h3></td>
								<td><h3>Status</h3></td>
									<td><h3>Date</h3></td>
									<td><h3>Time</h3></td>
									
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${model}" var="temp">
									<tr>
										<td>${temp.studentName}</td>
										<td>${temp.tutorName}</td>
										<td>${temp.status}</td>
										<td>${temp.date}</td>
										<td>${temp.time}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</form>
	</div>
</body>
</html>