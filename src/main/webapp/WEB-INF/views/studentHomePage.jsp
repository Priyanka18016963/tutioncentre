<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Home Page</title>
<script type="text/javascript">
	function displayBookClass() {
		document.getElementById("displaybookclass").style.display = "";
	}

	function displayProfile() {
		document.getElementById("displayprofile").style.display = "";
	}

	function displayChangeBooking() {
		document.getElementById("displaychangebooking").style.display = "";
	}

	function updatebooking(action) {
		if (action == "cancel") {
			document.getElementById("isCancel").value = "yes";
		}
		document.getElementById("bookingslistform").submit();
	}
	
	function displayBooksList(){
		document.getElementById("displaybooksList").style.display = "";
	}
	
	
</script>


</head>
<body>
	<div id="bookClass" class="bookClass" align="left">
		<h2>Welcome ${model.name}</h2>
		<table>
			<tr>
				<td>View Your Profile</td>
				<td><button type="button" onclick="displayProfile()">Profile</button></td>
			</tr>
			<tr>
				<td>Book a class</td>
				<td><button type="button" onclick="displayBookClass()">Book
						a class</button></td>
			</tr>
			<tr>
				<td>Change Booking</td>
				<td><button type="button" onclick="displayChangeBooking()">Update/Cancel
						Booking</button></td>
			</tr>
			<tr>
				<td>Books Requested</td>
				<td><button type="button" onclick="displayBooksList()">Books Requested</button></td>
			</tr>
			<tr>
				<td>${cancelConfirmed}</td>
				<td>&nbsp;</td>

			</tr>
		</table>
	</div>
	<div id="displaybookclass" class="displaybookclass"
		style="display: none">
		<form id="bookingForm" method="post" action="/bookClass">
			<label>Select the subject</label> <select id="subject" name="subject">
				<option value="None">-- Select --</option>
				<option value="1">English Comprehension</option>
				<option value="2">English Writing</option>
				<option value="3">Math</option>
				<option value="4">Numerical Reasoning</option>
				<option value="5">Verbal Reasoning</option>
				<option value="6">Non-verbal Reasoning</option>
			</select>
			<button type="submit">Submit</button>
		</form>
	</div>
	<div id="displayprofile" class="displayprofile" style="display: none">
		<table>
			<tr>
				<td>Name</td>
				<td>${model.name}</td>
			</tr>
			<tr>
				<td>Date of Birth</td>
				<td>${model.dob}</td>
			</tr>
			<tr>
				<td>Address</td>
				<td>${model.address}</td>
			</tr>
			<tr>
				<td>Contact Number</td>
				<td>${model.contactNumber}</td>
			</tr>
		</table>
	</div>
	<div id="displaychangebooking" class="displaychangebooking"
		style="display: none">
		<form id="bookingslistform" method="post" action="/updatebooking">
			<input type="hidden" name="isCancel" id="isCancel" value="no" />
			<c:choose>
				<c:when test="${empty model1}">
					<h2>No Bookings Are Available</h2>
				</c:when>
				<c:otherwise>
					<div class="changeBookingDiv" align="center">
						<h2>List of booked lessons</h2>
						<table class="table table-striped">
							<thead>
								<tr class="tr tr-success">
									<td><h3>Tutor Name</h3></td>
									<td><h3>Date</h3></td>
									<td><h3>Time</h3></td>
									<td><h3>Status</h3></td>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${model1}" var="temp">
									<tr>
										<td>${temp.tutorName}</td>
										<td>${temp.date}</td>
										<td>${temp.time}</td>
										<td>${temp.status}</td>
										<td><input type="radio" id="selectedSlot"
											name="selectedSlot"
											value=${temp.tutorName},${temp.date},${temp.time},${temp.status},${temp.subject}>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<input type="button" value="Update Booking"
							onclick="updatebooking('update')"> <input type="button"
							value="Cancel Booking" onclick="updatebooking('cancel')">
					</div>
				</c:otherwise>
			</c:choose>
		</form>
	</div>
	<div id="displaybooksList" class="displaybooksList"
		style="display: none">
		<form id="displaybooksform" method="post">
			<c:choose>
				<c:when test="${empty model2}">
					<h2>No Books Are Requested</h2>
				</c:when>
				<c:otherwise>
					<div class="changeBookingDiv" align="center">
						<h2>List of books</h2>
						<table class="table table-striped">
							<thead>
								<tr class="tr tr-success">
								<td><h3>Student Name</h3></td>
								<td><h3>Status</h3></td>
									<td><h3>Books</h3></td>
									
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${model2}" var="temp">
									<tr>
										<td>${temp.studentName}</td>
										<td>${temp.status}</td>
										<td>${temp.bookName}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</form>
	</div>
</body>
</html>