<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Report of Books</title>
</head>
<body>

<div id="displaybooksList" class="displaybooksList">
		<form id="displaybooksform" method="post" action="/updateBooksOrdered">
			<c:choose>
				<c:when test="${empty model}">
					<h2>No Books Are Requested</h2>
				</c:when>
				<c:otherwise>
					<div class="changeBookingDiv" align="center">
						<h2>List of books</h2>
						<table class="table table-striped">
							<thead>
								<tr class="tr tr-success">
								<td><h3>Student Name</h3></td>
								<td><h3>Status</h3></td>
									<td><h3>Books</h3></td>
									
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${model}" var="temp">
									<tr>
										<td>${temp.studentName}</td>
										<td>${temp.status}</td>
										<td>${temp.bookName}</td>
										<td><input type="radio" id="selectedSlot"
											name="selectedSlot"
											value=${temp.studentName}>
										</td>
										<td>
										<input type="submit" value="Mark as Ordered"></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:otherwise>
			</c:choose>
		</form>
	</div>
</body>
</html>