<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Home Page</title>
<script type="text/javascript">
	function displayLessons(input) {
		if (input == "B") {
			document.getElementById("adminhomepageform").action = "/retrieveBookedLessons";
		} else {
			document.getElementById("adminhomepageform").action = "/retrieveAttendedLessons";
		}
	}

	function submitPage() {
		document.getElementById("adminhomepageform").submit();
	}
</script>
</head>
<body>

	<div id="adminhomepage" class="adminhomepage" align="left">
		<form id="adminhomepageform" method="post">
			<h2>Welcome Admin</h2>
			<table>
				<tr>
					<td>Monthly Report for Booked Lessons</td>
				</tr>
				<tr>
					<td><input type="number" id="bookedlessons"
						name="bookedlessons" min="1" max="12"
						oninput="displayLessons('B')">Enter month(between 1 and
						12)</td>
				</tr>
				<tr>
				<tr>
					<td>Monthly Report for Attended,Cancelled and Changed Lessons</td>
				</tr>
				<tr>
					<td><input type="number" id="attendedlessons"
						name="attendedlessons" min="1" max="12"
						oninput="displayLessons('A')">Enter month(between 1 and
						12)</td>
				</tr>

				<tr>

					<td><button type="button" onclick="submitPage()">Submit</button></td>
				</tr>
			</table>
		</form>
	</div>

	<div id="displaybookslist" class="displaybookslist" align="left">
		<form id="displaybookslistform" method="post" action="/retrieveBooksList">
			<h3>Report for Books List</h3>
				<label>Select the Student Name</label> <select id="studentName" name="studentName">
				<option value="None">-- Select --</option>
				<option value="Jack">Jack</option>
				<option value="Jill">Jill</option>
				<option value="Tina">Tina</option>
				<option value="Mary">Mary</option>
				<option value="Adele">Adele</option>
				<option value="Ellen">Ellen</option>
				<option value="Amber">Amber</option>
				<option value="Emma">Emma</option>
				<option value="Chris">Chris</option>
				<option value="Bella">Bella</option>
			</select>
			<button type="submit">Submit</button>
		</form>
	</div>


</body>
</html>