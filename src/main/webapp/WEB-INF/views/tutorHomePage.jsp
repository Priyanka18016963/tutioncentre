<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Tutor Home Page</title>
<script type="text/javascript">
	function displayLessonsList() {
		document.getElementById("displaybookinglist").style.display = "";
	}
	
	function updatebooking(action) {
		if (action == "attended") {
			document.getElementById("isCancel").value = "yesno";
			document.getElementById("bookingslistform").action = "/updatebooking";
			document.getElementById("bookingslistform").submit();
		}else{
			document.getElementById("bookingslistform").action = "/displayBooksList";
			document.getElementById("bookingslistform").submit();
		}
	}
	
</script>
</head>
<body>

	<div id="tutorHome" class="tutorHome" align="left">
		<h2>Welcome ${model.name}</h2>
		<table>
			<tr>
				<td>Booked Lessons List</td>
				<td><button type="button" onclick="displayLessonsList()">Display
						Lessons List</button></td>
			</tr>
		</table>
	</div>

	<div id="displaybookinglist" class="displaybookinglist"	style="display: none">
		<form id="bookingslistform" method="post">
			<input type="hidden" name="isCancel" id="isCancel" value="no" />
			<c:choose>
				<c:when test="${empty model1}">
					<h2>No Bookings Are Available</h2>
				</c:when>
				<c:otherwise>
					<div class="changeBookingDiv" align="center">
						<h2>List of booked lessons</h2>
						<table class="table table-striped">
							<thead>
								<tr class="tr tr-success">
									<td><h3>Student Name</h3></td>
									<td><h3>Date</h3></td>
									<td><h3>Time</h3></td>
									<td><h3>Status</h3></td>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${model1}" var="temp">
									<tr>
										<td>${temp.studentName}</td>
										<td>${temp.date}</td>
										<td>${temp.time}</td>
										<td>${temp.status}</td>
										<td><input type="radio" id="selectedSlot"
											name="selectedSlot"
											value=${temp.studentName},${temp.date},${temp.time},${temp.status},${temp.subject}>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<input type="button" value="Mark as Attended"
							onclick="updatebooking('attended')"> 
							<input type="button"
							value="Add Books" onclick="updatebooking('addbooks')">
					</div>
				</c:otherwise>
			</c:choose>
		</form>
	</div>
</body>
</html>