package com.uh.tuitioncentre;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.uh.tuitioncentre.model.Lesson;
import com.uh.tuitioncentre.model.Student;
import com.uh.tuitioncentre.model.Tutor;
import com.uh.tuitioncentre.service.BookingService;
import com.uh.tuitioncentre.service.BooksRecordService;
import com.uh.tuitioncentre.service.LoginService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TuitioncentreApplication.class })
@Ignore
public class TuitioncentreControllerTest {

	@Autowired
	BookingService bookingService;

	@Autowired
	BooksRecordService booksRecordService;

	@Autowired
	LoginService loginService;

	@Test
	public void testBookingLesson() throws IOException, ParseException {
		Lesson lesson = bookingService.bookingLesson("1", "Jill");
		assertEquals(lesson.getStatus(), "Booked");
	}

	@Test
	public void testGetBooking() throws IOException, ParseException {
		List<Lesson> lessonsList = bookingService.getBookings("Jack", "S");
		for (Lesson lesson : lessonsList) {
			assertEquals(lesson.getStatus(), "Booked");
		}
	}

	@Test
	public void testGetBooksList() throws IOException, ParseException {
		List<String> lessonsList = booksRecordService.getBooksList("Jack");
		for (String books : lessonsList) {
			String oldBooks = books.split("/")[1];
			assertEquals(oldBooks, "Requested");

		}
	}

	@Test
	public void testStudentDetails() throws IOException, ParseException {
		Student student = loginService.getStudentDetails("Jack", "S");
		assertEquals(student.getAddress(), "Hatfield");
	}

	@Test
	public void testTutorDetails() throws IOException, ParseException {
		Tutor tutor = loginService.getTutorDetails("Tom", "T");
		assertEquals(tutor.getName(), "Tom");
	}

}
